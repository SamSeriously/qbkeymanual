# Create a vault

After connecting to your  QBKEY, the vault selection screen will be presented. Click the “Create or open a vault” button to continue.
Windows explorer will be opened. Select or add a directory on a location of choice (harddisk, usb-disk, cloud drive, …)

**We recommend to start with an empty folder (if you take a non empty folder, the data in the directory won’t get added and will be ignored).**

*Alternatively you can drag and drop your folder from the explorer window*

![1525959900424](assets/1525959900424.png)



# Open a vault

After connecting to your  QBKEY, the vault selection screen will be presented.
If you already have a vault you can choose your vault or connect the the last used vault (if enabled in the settings).
When you mark a vault as favorite you will be have the option to open your favorite vault here as well.

![1525959420292](assets/1525959420292.png)

###### NOTE!

You will only be able to see these first two options after you have enabled them in the settings screen (see [configuration](../setup/configuration/) for more information on this topic).

![1525960178953](assets/1525960178953.png)