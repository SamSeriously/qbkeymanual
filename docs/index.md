# Welcome to the QBKEY manual



If this is the first time you use the QBKEY we suggest following all the steps as described in the '[Setup section](setup/installation)'.

Returning users can choose a topic in the '[Usage section](usage/filevault)' to get more information about a certain part of the software.